import shelve


functions_codetext = {'absolute':'abs()'\
'delete attribute': 'delattr()'\

'hash': 'hash()'\

'memory view': 'memoryview()' \

'set': 'set()' \

'all': 'all()' \

'dict': 'dict()' \

'help': 'help()' \

'min': 'min()' \

'set atrribute': 'setattr()' \

'any': 'any()' \

'dir': 'dir()' \

'hex': 'hex()'\

'next': 'next()' \

'slice': 'slice()' \

 'ascii': 'ascii()'

 'divmod': 'divmod()' \

  'id': 'id()' \

   'id': 'object()' \

    'sorted': 'sorted()' \

     'bin': 'bin()' \

      'enumerate': 'enumerate()' \

       'input': 'input()' \

        'oct': 'oct()' \

         'staticmethod': 'staticmethod()' \

         'boolean': 'bool()' \

           'eval': 'eval()' \

            'int': 'int()' \

             'open': 'open()' \

             'string': 'str()' \

              'breakpoint': 'breakpoint()' \

                'exec': 'exec()'\

                 'is in instance': 'isinstance()' \

                 'ordered': 'ord()' \

                 'sum': 'sum()' \

                 'byte array': 'bytearray()' \

                 'filter': 'filter()' \

                 'is sub class': 'issubclass()' \

                 'power': 'pow()'\

                 'super': 'super()' \

                 'bytes': 'bytes()' \

                  'float': 'float()' \

                  'iterate': 'iter()' \

                  'print': 'print()'\

                  'tuple': 'tuple()'\

                  'callable': 'callable()' \

                  'format': 'format()' \

                   'length': 'len()' \

                   'property': 'property()' \

                   'type': 'type()' \

                   'charactor': 'chr()' \

                   'frozenset' 'frozenset()' \

                    'list': 'list()' \

                     'list': 'range()'\

                      'variables': 'vars()'}

'class method': 'classmethod()'\

'get attribhute': 'getattr()' \

'locals': 'locals()'\

'representation': 'repr()'

 'zip': 'zip()' \

  'compile': 'compile()' \

   'gloabals': 'globals()' \

    'map': 'map()' \

     'reversed': 'reversed()' \

      'import': '__import__()' \

      'complex': 'complex()' \

       'hass attribute': 'hasattr()' \

        'max': 'max()'\

        'round': 'round()'}



codetextdb = shelve.open('codetext.db', 'wb')
codetextdb['codetextdata'] = codetextdb
codetextdb.sync()
codetext.close()
